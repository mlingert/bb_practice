<?php

namespace Application\Form;

use Zend\InputFilter\InputFilter;
use Zend\Form\Form;

class LoginForm extends Form
{
    private $inputFilter;
    private $filters;

    public function __construct($name = null) {
        parent::__construct('login');
        
        $this->filters = [
            ['name' => 'StripTags'],
            ['name' => 'StringTrim']
        ];
        
        $this->add([
            'name' => 'firstname',
            'type' => 'Text',
            'required' => false,
            'filters' => $this->filters,
            'attributes' => [
                'style' => 'width: 300px'
            ]
        ]);

        $this->add([
            'name' => 'surname',
            'type' => 'Text',
            'required' => false,
            'filters' => $this->filters,
            'attributes' => [
                'style' => 'width: 300px'
            ]
        ]);

        $this->add([
            'name' => 'email',
            'type' => 'Text',
            'required' => true,
            'filters' => $this->filters,
            'attributes' => [
                'style' => 'width: 300px'
            ]
        ]);
        
        $this->add([
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => [
                'value' => 'Login',
                'class' => 'btn-success',
            ],
        ]);
    }

    public function getInputFilter() 
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            
            $inputFilter->add([
                'name' => 'email',
                'required' => true,
                'filters' => $this->filters,
                'validators' => [
                    ['name' => 'EmailAddress']
                ],
            ]);
            
            $this->inputFilter = $inputFilter;
        }
        
        return $this->inputFilter;
    }
}