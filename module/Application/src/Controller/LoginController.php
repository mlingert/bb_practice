<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Application\Form\LoginForm;

class LoginController extends AbstractActionController 
{
    public function indexAction() {
    	
        $form = new LoginForm();

        if ($this->getRequest()->isPost()) {

            $data = $this->getRequest()->getPost();
            $form->setInputFilter($form->getInputFilter());
            $form->setData($data);

            if ($form->isValid()) { 
                return ['data' => $data];
            } 
        }

        return ['form' => $form];
    }
}